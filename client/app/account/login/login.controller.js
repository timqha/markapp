'use strict';

export default class LoginController {
  user = {
    name: '',
    email: 'admin@example.com',
    password: 'admin'
  };
  errors = {
    login: undefined
  };
  submitted = false;


  /*@ngInject*/
  constructor(Auth, $state, $mdToast) {
    this.Auth = Auth;
    this.$state = $state;
    this.$mdToast = $mdToast;
  }

  login(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.login({
        email: this.user.email,
        password: this.user.password
      })
        .then(() => {
          // Logged in, redirect to dashboard
          this.$state.go('dashboard.message');
        })
        .catch(err => {
          this.$mdToast.show(this.$mdToast.simple().position('bottom left').textContent(err.message));
          // this.errors.login = err.message;
        });
    }
  }
}
