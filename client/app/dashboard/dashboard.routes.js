'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('dashboard', {
    url: '/dashboard',
    template: '<dashboard></dashboard>',
    authenticate: true
  })
  .state('dashboard.message', {
    url: '/message',
    template: require('./message/message.html'),
    controller: 'MessageController',
    controllerAs: 'vm',
    authenticate: true
  })
  .state('dashboard.statistic', {
    url: '/statistic',
    template: require('./statistic/statistic.html'),
    controller: 'StatisticController',
    controllerAs: 'vm',
    authenticate: true
  })
  .state('dashboard.permissions', {
    url: '/permission',
    template: require('./permissions/permission.html'),
    controller: 'PermissionController',
    controllerAs: 'vm',
    authenticate: true
  })
  .state('dashboard.settings', {
    url: '/settings',
    template: require('../account/settings/settings.html'),
    controller: 'SettingsController',
    controllerAs: 'vm',
    authenticate: true
  })
  .state('dashboard.admin', {
    url: '/admin',
    template: require('../admin/admin.html'),
    controller: 'AdminController',
    controllerAs: 'admin',
    authenticate: 'admin'
  });
}
