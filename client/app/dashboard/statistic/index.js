'use strict';

import angular from 'angular';
import StatisticController from './statistic.controller';

export default angular.module('maikAppApp.statistic', [])
  .controller('StatisticController', StatisticController)
  .name;
