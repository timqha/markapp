import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngMessages from 'angular-messages'
//TODO: delete mdPickers
// import mdPickers from 'mdPickers';
import routing from './dashboard.routes';
import message from './message';
import statistic from './statistic';
import permission from './permissions';
import settings from '../account/settings';
import admin from '../admin';
// TODO: delete smDateTime
// import {smDateTimeRangePicker} from 'smdatetimerangepicker';
// import {ngMaterialDatePicker} from 'angular-material-datetimepicker';

export class DashBoardController {

  constructor(Auth){
    'ngInject';

    this.isAdmin = Auth.isAdminSync;

    // TODO: Переместить данные в сервис.
    this.menuItems = [
      {
        name: 'Message',
        icon: 'message',
        sref: '.message'
      },
      {
        name: 'Statistics',
        icon: 'equalizer',
        sref: '.statistic'
      },
      {
        name: 'Permissions',
        icon: 'person',
        sref: '.permissions'
      },
      {
        name: 'Settings',
        icon: 'settings',
        sref: '.settings'
      }
    ];
  }

}

var options = [
  uiRouter,
  message,
  statistic,
  permission,
  settings,
  admin,
  ngMessages
];

export default angular.module('maikAppApp.dashboard', options)
  .config(routing)
  .component('dashboard', {
    template: require('./dashboard.html'),
    controller: DashBoardController,
    controllerAs: 'vm'
  })
  .name;
