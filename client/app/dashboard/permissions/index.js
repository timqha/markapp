'use strict';

import angular from 'angular';
import PermissionController from './permission.controller';

export default angular.module('maikAppApp.permissions', [])
  .controller('PermissionController', PermissionController)
  .name;
