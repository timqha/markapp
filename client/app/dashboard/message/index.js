'use strict';

import angular from 'angular';
import MessageController from './message.controller';

export default angular.module('maikAppApp.message', [])
  .controller('MessageController', MessageController)
  .name;
