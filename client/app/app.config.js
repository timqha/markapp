'use strict';

export function routeConfig($urlRouterProvider, $locationProvider) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode(true);
}

export function themeConfig($mdThemingProvider) {
  'ngInject';

  $mdThemingProvider
    .theme('indigo')
    .primaryPalette('teal')
    .accentPalette('grey')
    .warnPalette('red')
    .backgroundPalette('grey');
}

