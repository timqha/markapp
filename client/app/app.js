'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import ngMaterial from 'angular-material';
import nvd3 from 'angular-nvd3';
// import moment from 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js';
// import moment from 'moment';
// import * as moment from 'moment';
// import moment from 'moment/min/moment.min';
// import
// import * as moment from 'moment';

// var moment = require('moment');

// <script src="bower_components/d3/d3.js"></script>
//   <script src="bower_components/nvd3/nv.d3.js"></script> <!-- or use another assembly -->
// <script src="bower_components/angular-nvd3/dist/angular-nvd3.js"></script>
//   <link rel="stylesheet" href="bower_components/nvd3/nv.d3.css">
//
import uiRouter from 'angular-ui-router';

// import ngMessages from 'angular-messages';
// import ngValidationMatch from 'angular-validation-match';


import {
  routeConfig,
  themeConfig
} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import dashboard from './dashboard/dashboard.component';


import './app.scss';

angular.module('maikAppApp', [ngCookies, ngResource, ngSanitize, uiRouter, ngMaterial, nvd3,
  _Auth, account, admin,
    navbar, footer, main, constants, util, dashboard
  ])
  .config(routeConfig)
  .run(function($rootScope, $location, Auth) {
    'ngInject';

    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function(event, next) {
      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  })
  .config(themeConfig);


angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['maikAppApp'], {
      strictDi: true
    });
  });
