'use strict';

import express from 'express';
import passport from 'passport';
import {setTokenCookie} from '../auth.service';

var router = express.Router();

router
  .get('/', passport.authenticate('vkontakte', {
    failureRedirect: '/signup',
    scope: [
      'profile',
      'email',
      'friends',
      'wall',
      'groups',
      'offline',
      'stats',
      'photos',
      'video',
      'audio'
    ],
    session: true
  }))
  .get('/callback', passport.authenticate('vkontakte', {
    failureRedirect: '/signup',
    session: false
  }), setTokenCookie);

export default router;
