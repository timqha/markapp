import passport from 'passport';
import {Strategy as VKontakteStrategy} from 'passport-vkontakte';

export function setup(User, config) {
  passport.use(new VKontakteStrategy({
    clientID: config.vkontakte.clientID,
    clientSecret: config.vkontakte.clientSecret,
    callbackURL: config.vkontakte.callbackURL,
    scope: [
        'profile',
        'email'
      ]
  },
  function(accessToken, refreshToken, profile, email, done) {
    // params, можно попробовать его вместо email
    console.log("CONSOLE");
    console.dir(profile);
    console.dir(email);
    console.log("CONSOLEEnd");
    console.dir(accessToken);
    console.log("CONSOLEddd");
    User.findOne({'vkontakte.id': profile.id}).exec()
      .then(user => {
        if(user) {
          return done(null, user);
        }

        user = new User({
          name: profile.name.givenName,
          email: email.email,
          role: 'user',
          // username: profile.emails[0].value.split('@')[0],
          username: profile.username,
          provider: 'vkontakte',
          vkontakte: profile._json
        });
        user.save()
          .then(savedUser => done(null, savedUser))
          .catch(err => done(err));
      })
      .catch(err => done(err));
  }));
}
